<?php

namespace Drupal\tmgmt_microsoft;

use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Microsoft translator UI.
 */
class MicrosoftTranslatorUi extends TranslatorPluginUiBase {

  /**
   * The translator plugin.
   *
   * @var \Drupal\tmgmt_microsoft\Plugin\tmgmt\Translator\MicrosoftTranslator
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    $instruction_url = 'https://learn.microsoft.com/azure/ai-services/translator/translator-text-apis?tabs=csharp';
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Microsoft Azure API Key'),
      '#default_value' => $translator->getSetting('api_key'),
      '#required' => TRUE,
      '#description' => t('Please enter your Azure Cognitive Services API key. Instructions on how to set up your Azure account and get the key can be found <a href="@link" target="_blank">here</a>', ['@link' => $instruction_url]),
    ];
    $form += parent::addConnectButton();

    return $form;
  }

  /**
   * Get the translator plugin.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   The translator entity.
   *
   * @return \Drupal\tmgmt_microsoft\Plugin\tmgmt\Translator\MicrosoftTranslator
   *   The translator plugin.
   */
  protected function getPlugin(TranslatorInterface $translator) {
    if (!isset($this->plugin)) {
      $this->plugin = $translator->getPlugin();
    }
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $api_key = $form_state->getValue(['settings', 'api_key']);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    $plugin = $this->getPlugin($translator);
    try {
      $plugin->getToken($api_key);
    }
    catch (TMGMTException $e) {
      $form_state->setErrorByName('settings][api_key', t('The "Azure API Key" is not valid.'));
    }
  }

}

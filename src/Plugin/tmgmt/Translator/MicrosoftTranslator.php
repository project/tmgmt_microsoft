<?php

namespace Drupal\tmgmt_microsoft\Plugin\tmgmt\Translator;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tmgmt\ContinuousTranslatorInterface;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\tmgmt\Translator\TranslatableResult;

/**
 * Microsoft translator plugin.
 *
 * Check @link https://docs.microsoft.com/en-us/azure/cognitive-services/translator/reference/v3-0-translate
 * Microsoft Translator @endlink.
 *
 * @TranslatorPlugin(
 *   id = "microsoft",
 *   label = @Translation("Microsoft"),
 *   description = @Translation("Microsoft Translator service."),
 *   ui = "Drupal\tmgmt_microsoft\MicrosoftTranslatorUi",
 *   logo = "icons/microsoft.svg",
 * )
 */
class MicrosoftTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface, ContinuousTranslatorInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Maximum supported characters.
   *
   * @var int
   *
   * @see https://docs.microsoft.com/en-us/azure/cognitive-services/translator/reference/v3-0-translate?tabs=curl#request-body
   */
  protected $maxCharacters = 50000;

  /**
   * {@inheritdoc}
   */
  protected $escapeStart = '<span class="notranslate">';

  /**
   * {@inheritdoc}
   */
  protected $escapeEnd = '</span>';

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The JWT tokens.
   *
   * @var array
   */
  protected $tokens = [];

  /**
   * Constructs a LocalActionBase object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle HTTP client.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(ClientInterface $client, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('http_client'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($key = $translator->getSetting('api_key')) {
      try {
        $this->getToken($key);
        return AvailableResult::yes();
      }
      catch (TMGMTException $e) {
        return AvailableResult::no(t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
          '@translator' => $translator->label(),
          ':configured' => $translator->toUrl()->toString(),
        ]));
      }

    }
    return AvailableResult::no(t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function checkTranslatable(TranslatorInterface $translator, JobInterface $job) {
    foreach ($job->getItems() as $item) {
      foreach (\Drupal::service('tmgmt.data')->filterTranslatable($item->getData()) as $value) {
        // If one of the texts in this job item exceeds the max character count
        // the job can't be translated.
        $count = mb_strlen($value['#text']);
        if ($count > $this->maxCharacters) {
          return TranslatableResult::no($this->t('The character length (@length) of the job exceeds the max character count (@max).', [
            '@length' => $count,
            '@max' => $this->maxCharacters,
          ]));
        }
      }
    }

    return parent::checkTranslatable($translator, $job);
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $this->requestJobItemsTranslation($job->getItems());
    if (!$job->isRejected()) {
      $job->submitted('The translation job has been submitted.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $languages = [];
    // Prevent access if the translator isn't configured yet.
    if (!$this->checkAvailable($translator)) {
      // @todo should be implemented by an Exception.
      return $languages;
    }
    try {
      $request = $this->doLanguageRequest();
      if ($request) {
        // @codingStandardsIgnoreStart
        // Response looks like:
        // {
        //  "translation": {
        //    ...
        //    "en": {
        //      "name": "English",
        //      "nativeName": "English",
        //      "dir": "ltr"
        //    },
        //    "es": {
        //      "name": "Spanish",
        //      "nativeName": "Espa\u00f1ol",
        //      "dir": "ltr"
        //    },
        //    ...
        //  }
        // }
        // @codingStandardsIgnoreEnd
        $response = json_decode((string) $request->getBody(), TRUE);
        foreach ($response['translation'] as $langcode => $names) {
          $languages[$langcode] = $names['name'];
        }
      }
    }
    catch (BadResponseException $e) {
      $this->messenger()->addMessage($this->t('Cannot get languages from the translator: %message', ['%message' => $e->getMessage()]), 'error');
      return $languages;
    }

    return $languages;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRemoteLanguagesMappings() {
    return [
      'zh-hans' => 'zh-CHS',
      'zh-hant' => 'zh-CHT',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTargetLanguages(TranslatorInterface $translator, $source_language) {
    $remote_languages = $this->getSupportedRemoteLanguages($translator);

    // There are no language pairs, any supported language can be translated
    // into the others. If the source language is part of the languages,
    // then return them all, just remove the source language.
    if (array_key_exists($source_language, $remote_languages)) {
      unset($remote_languages[$source_language]);
      return $remote_languages;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasCheckoutSettings(JobInterface $job) {
    return FALSE;
  }

  /**
   * Execute a request against the Microsoft API.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The HTTP response.
   */
  protected function doLanguageRequest() {
    $url = $this->getConfiguration()['translate url'] . '/languages';
    $request_url = Url::fromUri($url)->toString();
    $headers = [
      'X-ClientTraceId' => \Drupal::service('uuid')->generate(),
    ];
    $query = [
      'api-version' => '3.0',
      'scope' => 'translation',
    ];
    return $this->client->request('GET', $request_url, [
      'headers' => $headers,
      'query' => $query,
    ]);
  }

  /**
   * Execute a request against the Microsoft API.
   *
   * @param \Drupal\tmgmt\Entity\Translator $translator
   *   The translator entity to get the settings from.
   * @param string $from
   *   The source language.
   * @param string $to
   *   The destination language.
   * @param string $text
   *   Text to be translated.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The HTTP response.
   */
  protected function doTranslateRequest(Translator $translator, $from, $to, $text) {
    $api_key = $translator->getSetting('api_key');
    if (empty($api_key)) {
      return NULL;
    }
    $url = $this->getConfiguration()['translate url'] . '/translate';
    $json = json_encode([
      ['Text' => $text],
    ]);
    $token = $this->getToken($api_key);
    $request_url = Url::fromUri($url)->toString();
    $headers = [
      'X-ClientTraceId' => \Drupal::service('uuid')->generate(),
      'Authorization' => "Bearer $token",
      'Content-Type' => 'application/json',
    ];
    $query = [
      'textType' => 'html',
      'from' => $from,
      'to' => $to,
      'api-version' => '3.0',
    ];
    $options = [
      'headers' => $headers,
      'query' => $query,
      'body' => $json,
    ];
    return $this->client->request('POST', $request_url, $options);
  }

  /**
   * Get the access token.
   *
   * @param string $api_key
   *   Microsoft Azure API key.
   *
   * @return string
   *   The access token.
   *
   * @throws \Drupal\tmgmt\TMGMTException
   *   Thrown when the API key is missing or not valid.
   */
  public function getToken($api_key) {
    if (!empty($this->tokens[$api_key])) {
      return $this->tokens[$api_key];
    }

    if (!$api_key) {
      throw new TMGMTException('Missing API key.');
    }

    $url = $this->getConfiguration()['token url'];
    $headers = [
      'Content-Type' => 'application/json',
      'Accept' => 'application/jwt',
      'Ocp-Apim-Subscription-Key' => $api_key,
    ];

    try {
      $response = $this->client->request('POST', $url, ['headers' => $headers]);
    }
    catch (BadResponseException $e) {
      $error = json_decode((string) $e->getResponse()->getBody());
      $message = property_exists($error, 'message') ? $error->message : 'n/a';
      throw new TMGMTException('Microsoft Translate service returned the following error: @error', ['@error' => $message]);
    }
    return $this->tokens[$api_key] = (string) $response->getBody();
  }

  /**
   * {@inheritdoc}
   */
  public function requestJobItemsTranslation(array $job_items) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = reset($job_items)->getJob();
    /** @var \Drupal\tmgmt\Entity\JobItem $job_item */
    foreach ($job_items as $job_item) {
      if ($job->isContinuous()) {
        $job_item->active();
      }
      // Pull the source data array through the job and flatten it.
      $data = \Drupal::service('tmgmt.data')->filterTranslatable($job_item->getData());
      $translation = [];
      foreach ($data as $key => $value) {
        // Query the translator API.
        try {
          $result = $this->doTranslateRequest($job->getTranslator(), $job->getRemoteSourceLanguage(), $job->getRemoteTargetLanguage(), $this->escapeText($value));
          // Response is a JSON string like the following:
          // Original string: Hello, what is your name?
          // @codingStandardsIgnoreStart
          // [{"translations":[{"text":"Hola, ¿cómo te llamas?","to":"es"}]}]
          // @codingStandardsIgnoreEnd
          $response = json_decode((string) $result->getBody(), TRUE);
          $translation[$key]['#text'] = $this->unescapeText($response[0]['translations'][0]['text']);

          // Save the translated data through the job.
          $job_item->addTranslatedData(\Drupal::service('tmgmt.data')->unflatten($translation));

        }
        catch (RequestException $e) {
          $job->rejected('Rejected by Microsoft Translator: @error', ['@error' => (string) $e->getResponse()->getBody()], 'error');
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'translate url' => 'https://api.cognitive.microsofttranslator.com',
      'token url' => 'https://api.cognitive.microsoft.com/sts/v1.0/issueToken/',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

}

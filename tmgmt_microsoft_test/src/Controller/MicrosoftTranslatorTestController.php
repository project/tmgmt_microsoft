<?php

namespace Drupal\tmgmt_microsoft_test\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Mock services for Microsoft translator.
 */
class MicrosoftTranslatorTestController {

  /**
   * Helper to trigger mock response error.
   *
   * @param string $domain
   *   The domain.
   * @param string $reason
   *   The reason.
   * @param string $message
   *   The message.
   * @param string $locationType
   *   The location type.
   * @param string $location
   *   The location.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON encoded error response.
   */
  public function triggerErrorResponse($domain, $reason, $message, $locationType = NULL, $location = NULL) {
    $response = [
      'error' => [
        'errors' => [
          'domain' => $domain,
          'reason' => $reason,
          'message' => $message,
        ],
        'code' => 400,
        'message' => $message,
      ],
    ];

    if (!empty($locationType)) {
      $response['error']['errors']['locationType'] = $locationType;
    }
    if (!empty($location)) {
      $response['error']['errors']['location'] = $location;
    }

    return new JsonResponse($response, 400);
  }

  /**
   * Page callback for getting the supported languages.
   */
  public function getLanguages(Request $request) {
    $query = $request->query;
    if (!$query->has('api-version')) {
      return new Response('Bad request - missing: api-version', 400);
    }
    $json['translation']['de'] = [
      'name' => 'German',
      'nativeName' => 'German',
      'dir' => 'ltr',
    ];
    $json['translation']['en'] = [
      'name' => 'English',
      'nativeName' => 'English',
      'dir' => 'ltr',
    ];
    $json['translation']['es'] = [
      'name' => 'Spanish',
      'nativeName' => 'Espanol',
      'dir' => 'ltr',
    ];
    return new JsonResponse($json);
  }

  /**
   * Page callback for providing the access token.
   */
  public function serviceToken(Request $request) {
    $headers = $request->headers;
    if (!$headers->has('content-type')) {
      return $this->triggerErrorResponse('global', 'required', 'Required header: Content-Type', 'header', 'Content-Type');
    }
    elseif (!$headers->has('accept') && !$headers->get('content-type') == 'application/json') {
      return $this->triggerErrorResponse('global', 'required', 'Required header: Accept', 'header', 'Accept');
    }
    elseif (!$headers->has('ocp-apim-subscription-key')) {
      return $this->triggerErrorResponse('global', 'required', 'Required header: Ocp-Apim-Subscription-Key', 'header', 'Ocp-Apim-Subscription-Key');
    }

    if ($headers->get('ocp-apim-subscription-key') != 'correct api_key') {
      $response = [];
      $response['statusCode'] = 403;
      $response['message'] = 'Access denied due to invalid subscription key. Make sure to provide a valid key for an active subscription.';
      return new JsonResponse($response, 403, ['status' => 'Invalid token']);

    }
    return new Response('correct token');
  }

  /**
   * Simulate a translation sent back to plugin.
   */
  public function translate(Request $request) {
    $headers = $request->headers;
    $query = $request->query;
    if (!$headers->has('authorization') || $headers->get('authorization') != 'Bearer correct token') {
      return new Response('Forbidden', 403, ['status' => 'Invalid token']);
    }
    elseif (!$headers->has('content-type') && !$headers->get('content-type') == 'application/json') {
      return new Response('Bad request - wrong Content-Type', 400);
    }
    elseif (!$query->has('to')) {
      return new Response('Bad request - missing: to', 400);
    }
    elseif (!$query->has('from')) {
      return new Response('Bad request - missing: from', 400);
    }
    elseif (!$query->has('textType')) {
      return new Response('Bad request - missing: textType', 400);
    }
    elseif (!$query->has('api-version')) {
      return new Response('Bad request - missing: api-version', 400);
    }

    $json[0]['translations'][0]['text'] = 'Hallo Welt';
    $json[0]['translations'][0]['to'] = 'es';
    return new JsonResponse($json);
  }

}

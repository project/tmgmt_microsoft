<?php

namespace Drupal\Tests\tmgmt_microsoft\Functional;

use Drupal\Core\Form\FormState;
use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt_microsoft\MicrosoftTranslatorUi;
use Drupal\tmgmt_microsoft\Plugin\tmgmt\Translator\MicrosoftTranslator;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Http\Message\ResponseInterface;

/**
 * Basic tests for the Microsoft translator.
 *
 * @group tmgmt_microsoft
 */
class MicrosoftTest extends BrowserTestBase {

  /**
   * A tmgmt_translator with a server mock.
   *
   * @var \Drupal\tmgmt\Entity\Translator
   */
  protected $translator;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'tmgmt',
    'tmgmt_test',
    'tmgmt_microsoft',
    'tmgmt_microsoft_test',
    'node',
    'block',
    'locale',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'administer languages',
      'access administration pages',
      'administer content types',
      'administer tmgmt',
    ]);
    $this->drupalLogin($this->user);

    $this->addLanguage('de');
    $this->translator = $this->createTranslator(['plugin' => 'microsoft']);
  }

  /**
   * Tests basic API methods of the plugin.
   */
  public function testMicrosoftTranslate(): void {
    $job = $this->createJob();
    $job->translator = $this->translator->id();
    $job->save();
    $item = $job->addItem('test_source', 'test', '1');
    $item->save();

    $expected = t('@label can not translate from English to German.', ['@label' => $this->translator->label()]);
    $reason = $this->translator->checkTranslatable($job)->getReason();
    $this->assertEquals($expected, $reason, 'Check if the translator is not available at this point because we did not define the API parameters.');

    // Save a wrong client ID.
    $translator = $job->getTranslator();
    $translator->setSetting('api_key', 'wrong api_key');
    $translator->save();
    $job->requestTranslation();
    // The translations should fail.
    foreach ($job->getItems() as $item) {
      $this->assertFalse($item->isNeedsReview());
    }

    // Save a correct client ID.
    $translator->setSetting('api_key', 'correct api_key');
    $translator->save();
    $configuration = [
      'translate url' => Url::fromUri('base:tmgmt_microsoft_mock')->setAbsolute()->toString(),
      'token url' => Url::fromRoute('tmgmt_microsoft_test.token')->setAbsolute()->toString(),
    ];
    $translator_plugin = new MicrosoftTranslator($this->container->get('http_client'), $configuration, 'test', []);

    $languages = $translator_plugin->getSupportedRemoteLanguages($translator);
    $this->assertArrayHasKey('de', $languages);
    $this->assertArrayHasKey('en', $languages);
    $this->assertArrayHasKey('es', $languages);

    // Make sure the translator returns the correct supported target languages.
    $languages = $translator_plugin->getSupportedTargetLanguages($translator, 'en');
    $this->assertArrayHasKey('de', $languages);
    $this->assertArrayHasKey('es', $languages);
    $this->assertArrayNotHasKey('en', $languages);

    $this->assertTrue($translator_plugin->checkTranslatable($job->getTranslator(), $job)->getSuccess());

    $translator_plugin->requestJobItemsTranslation($job->getItems());
    foreach ($job->getItems() as $item) {
      $data = $item->getData();
      $this->assertEquals('Hallo Welt', $data['dummy']['deep_nesting']['#translation']['#text']);
    }

    // Test continuous integration.
    $this->config('tmgmt.settings')
      ->set('submit_job_item_on_cron', TRUE)
      ->save();

    // Continuous settings configuration.
    $continuous_settings = [
      'content' => [
        'node' => [
          'enabled' => 1,
          'bundles' => [
            'test' => 1,
          ],
        ],
      ],
    ];

    $continuous_job = $this->createJob('en', 'de', 0, [
      'label' => 'Continuous job',
      'job_type' => JobInterface::TYPE_CONTINUOUS,
      'translator' => $this->translator,
      'continuous_settings' => $continuous_settings,
    ]);
    $continuous_job->save();
    // Create an english node.
    $node = $this->createNode([
      'title' => $this->randomMachineName(),
      'uid' => 0,
      'type' => 'test',
      'langcode' => 'en',
    ]);
    $node->save();
    $continuous_job->addItem('test_source', $node->getEntityTypeId(), $node->id());

    foreach ($continuous_job->getItems() as $item) {
      $this->assertEquals(JobItemInterface::STATE_INACTIVE, $item->getState());
    }

    $translator_plugin->requestJobItemsTranslation($continuous_job->getItems());
    foreach ($continuous_job->getItems() as $item) {
      $data = $item->getData();
      $this->assertEquals('Hallo Welt', $data['dummy']['deep_nesting']['#translation']['#text']);
      $this->assertEquals(JobItemInterface::STATE_REVIEW, $item->getState());
    }
  }

  /**
   * Tests the UI validation.
   */
  public function testMicrosoftUiValidation(): void {
    $translator_ui = new MicrosoftTranslatorUiStub([], 'test', []);
    $form = [];
    $form_state = new FormState();
    $form_state->setFormObject(\Drupal::entityTypeManager()->getFormObject('tmgmt_translator', 'edit'));
    $form_state->getFormObject()->setEntity($this->translator);
    /** @var \Drupal\tmgmt_microsoft\Plugin\tmgmt\Translator\MicrosoftTranslator $plugin */
    $plugin = $this->translator->getPlugin();
    $plugin->setConfiguration(['token url' => Url::fromRoute('tmgmt_microsoft_test.token')->setAbsolute()->toString()]);
    $translator_ui->setPlugin($plugin);

    // Should work.
    $form_state->setValue(['settings', 'api_key'], 'correct api_key');
    $translator_ui->validateConfigurationForm($form, $form_state);
    $this->assertEmpty($form_state->getErrors());

    // Should fail.
    $form_state->setValue(['settings', 'api_key'], 'wrong api_key');
    $translator_ui->validateConfigurationForm($form, $form_state);
    $this->assertEquals('The "Azure API Key" is not valid.', $form_state->getErrors()['settings][api_key']);
  }

  /**
   * Tests the test controller.
   */
  public function testMicrosoftTestController(): void {
    $bad_key = 'wrong api_key';
    $good_key = 'correct api_key';
    $good_token = 'correct token';

    $languages = [
      'route' => 'tmgmt_microsoft_test.languages',
      'verb' => 'GET',
      'key' => $good_key,
      'query' => [
        'scope' => 'translation',
        'api-version' => '3.0',
      ],
    ];
    $response = $this->assertHttpRequest('languages', $languages);
    $json = json_decode((string) $response->getBody(), TRUE);
    $this->assertArrayHasKey('de', $json['translation']);
    $this->assertArrayHasKey('en', $json['translation']);
    $this->assertArrayHasKey('es', $json['translation']);

    $token = [
      'route' => 'tmgmt_microsoft_test.token',
      'verb' => 'POST',
      'key' => $bad_key,
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/jwt',
        'Ocp-Apim-Subscription-Key' => $bad_key,
      ],
    ];
    $response = $this->assertHttpRequest('bad_token', $token);
    $json = json_decode((string) $response->getBody(), TRUE);
    $this->assertArrayHasKey('message', $json);

    $token['key'] = $good_key;
    $token['headers']['Ocp-Apim-Subscription-Key'] = $good_key;
    $response = $this->assertHttpRequest('good_token', $token);
    $this->assertSame('correct token', (string) $response->getBody());

    $translate = [
      'route' => 'tmgmt_microsoft_test.translate',
      'verb' => 'POST',
      'key' => $bad_key,
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer $bad_key",
      ],
      'query' => [
        'from' => 'en',
        'to' => 'es',
        'textType' => 'plain',
        'api-version' => '3.0',
      ],
      'body' => '{"text":"Hello world"}',
    ];
    $this->assertHttpRequest('bad_translate', $translate);

    $translate['key'] = $good_key;
    $translate['headers']['Authorization'] = "Bearer $good_token";
    $response = $this->assertHttpRequest('good_translate', $translate);
    $json = json_decode((string) $response->getBody(), TRUE);
    $this->assertSame('Hallo Welt', $json[0]['translations'][0]['text']);
  }

  /**
   * Assert an http request is called successfully.
   *
   * @param string $label
   *   Label for http request.
   * @param array $options
   *   The http request options.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The http response.
   */
  protected function assertHttpRequest($label, array $options): ResponseInterface {
    $url = Url::fromRoute($options['route']);
    $verb = $options['verb'];
    $key = $options['key'];
    foreach (['route', 'verb', 'key'] as $delta) {
      unset($options[$delta]);
    }
    if ($key == 'wrong api_key') {
      $exception_thrown = FALSE;
      try {
        $this->container->get('http_client')->request($verb, $url->setAbsolute()->toString(), $options);
      }
      catch (BadResponseException $e) {
        $exception_thrown = TRUE;
        $response = $e->getResponse();
      }
      $this->assertTrue($exception_thrown, "No exception thrown for $label");
    }
    else {
      $response = $this->container->get('http_client')->request($verb, $url->setAbsolute()->toString(), $options);
      $this->assertSame($response->getStatusCode(), 200);
    }
    return $response;
  }

  /**
   * Sets the proper environment.
   *
   * Currently just adds a new language.
   *
   * @param string $langcode
   *   The language code.
   */
  protected function addLanguage($langcode): void {
    $language = ConfigurableLanguage::createFromLangcode($langcode);
    $language->save();
  }

  /**
   * Creates, saves and returns a translator.
   *
   * @return \Drupal\tmgmt\TranslatorInterface
   *   The newly created translator.
   */
  protected function createTranslator(array $values = []): TranslatorInterface {
    $translator = Translator::create($values + [
      'name' => strtolower($this->randomMachineName()),
      'label' => $this->randomMachineName(),
      'plugin' => 'test_translator',
      'remote_languages_mappings' => [],
      'settings' => empty($values['plugin']) ? [
        'key' => $this->randomMachineName(),
        'another_key' => $this->randomMachineName(),
      ] : [],
    ]);
    $this->assertSame(SAVED_NEW, $translator->save());
    return $translator;
  }

  /**
   * Creates, saves and returns a translation job.
   *
   * @return \Drupal\tmgmt\JobInterface
   *   The newly created job.
   */
  protected function createJob($source = 'en', $target = 'de', $uid = 1, $values = []): JobInterface {
    $job = tmgmt_job_create($source, $target, $uid, $values);
    $this->assertSame(SAVED_NEW, $job->save());

    // Assert that the translator was assigned a tid.
    $this->assertGreaterThan(0, $job->id());
    return $job;
  }

}

/**
 * Stub class to set plugin.
 */
class MicrosoftTranslatorUiStub extends MicrosoftTranslatorUi {

  /**
   * Set the plugin.
   *
   * @param \Drupal\tmgmt_microsoft\Plugin\tmgmt\Translator\MicrosoftTranslator $plugin
   *   The plugin.
   */
  public function setPlugin(MicrosoftTranslator $plugin): void {
    $this->plugin = $plugin;
  }

}
